r = zeros(5);
for i = 1:5
    r(i) = power(0.8, abs(i-1)) + power(0.5, 2*abs(i-1));
end
Rg = zeros(3,3);
for i = 1:3
    for j = 1:3
        Rg(i,j) = r(abs(i-j)+1);
    end
end
R2 = Rg([1:2],[1:2]);
r2 = r([2:3]);
R3 = Rg([1:3],[1:3]);
r3 = r([2:4]);


a2=-inv(R2)*transpose(r2);
a3=-inv(R3)*transpose(r3);

a2
a3

err2=r(1)+(transpose(a2)*transpose(r2))
err2
err3=r(1)+(transpose(a3)*transpose(r3))
err3