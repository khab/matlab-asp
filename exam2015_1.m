r = [17 4 3]
Rg = zeros(3,3)
for i=1:3
    for j=1:3
        Rg(i,j)=r(abs(i-j)+1);
    end
end

[V,D] = eig(Rg)

a = (V([1:3],1))/(V(1,1))
lam_min = D(1)
a_roots = roots(transpose(a));
my_root = angle(a_roots(1));
P = r(1) - D(1)
Ampl = sqrt((2*P))
SNR = P / D(1)