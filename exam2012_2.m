[rx,l]=xcorr([1 -1 0 2 3 2 -4 1],2,'biased')
Rg = zeros(2,2);
for i = 1:2
    for j = 1:2
        Rg(i,j) = rx(abs(i-j)+3);
    end
end
r = rx([4:5]);
a = -inv(Rg)*transpose(r);

d02=rx(3)+(transpose(a)*transpose(r))
d0=sqrt(d02)

d0
