rx = zeros(4)
for i = 1:3
    rx(i) = power(0.6,i-1);
end
Rg = zeros(3,3)
for i = 1:3
    for j = 1:3
          Rg(i,j) = rx(abs(i-j)+1);
    end
end
r = transpose(rx);
Rg

sx = [3 2 1];
s = transpose(sx);
kappa = 1 /sqrt(sx*inv(Rg)*s);

h = kappa*inv(Rg)*s;
SNR_opt = 1 / power(kappa,2)

hnt = [1 0 1]
hn = transpose(hnt)
SNR_hn = (power((hnt*s),2))/(hnt*inv(Rg)*hn)
%freqz(h)