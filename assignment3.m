mux = 0; sigmax= 1; size = 5000;
x = sigmax * randn(1,size) + mux;
muv = 0; sigmav = square(0.2);
v = sigmav * randn(1,size) + muv;
w = (3*x) + circshift(x, [0,-1]);
y = w + v;
rx = xcorr(x, x);
rxy = xcorr(x,y);
p = 2;
RMx = zeros(p,p);
for i = 1:p
    for j = 1:p
        RMx(i,j) = rx(i-j+size)/size;
    end
end
GMxyt = zeros(1,p);
for i = 1:p
    GMxyt(i) = rxy(i+size)/size;
end
GMxy = transpose(GMxyt);

ys = y.^2;
Eys = mean(ys);

ht = [5.7, -4.3];
h = transpose(ht);
Ees = Eys - (2*ht*GMxy) + (ht*RMx)*h;

